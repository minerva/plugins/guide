require('../css/styles.css');
const introJs = require('intro.js');
const initializeStructureStory = require('./structures');
const initializeIntroStory = require('./introduction');
const initializeDrugsStory = require('./drugs');

const pluginName = 'guide';
const pluginLabel = 'PD map guide';

const pluginVersion = '1.0.2';

const settings = {
    disableInteraction: false
    , padding: 30
    , inactivityTimeout: 600000
    , confirmationTimeout: 30000
    , confirmationTimeoutRefresh: 1000
};

const selections = require('./selections');

let $ = window.$;
if ($ === undefined) {
    $ = minerva.$;
}

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let $container;
let minervaProxy;
let pluginContainer;
let pluginContainerId;
let mainModelId = undefined;

let introJsInstances = [];

let guideGlobalState = {val:undefined};

let mainTimeoutId;
let confirmationTimeoutId;
let currentConfirmationTimeout = settings.confirmationTimeout;


const register = function(_minerva) {

    // console.log('registering ' + pluginName + ' plugin');

    // $(".tab-content").css('position', 'relative');

    minervaProxy = _minerva;
    pluginContainer = $(minervaProxy.element);
    pluginContainerId = pluginContainer.attr('id');
    // console.log('pluginContainer', pluginContainer);

    // console.log('minerva object ', minervaProxy);
    // console.log('project id: ', minervaProxy.project.data.getProjectId());
    // console.log('model id: ', minervaProxy.project.data.getModels()[0].modelId);

    initPlugin();
};

const unregister = function () {

    clearTimeouts();
    // console.log('unregistering ' + pluginName + ' plugin');

    // unregisterListeners();
    // return deHighlightAll();
};

const getName = function() {
    return pluginLabel;
};

const getVersion = function() {
    return pluginVersion;
};

/**
 * Function provided by Minerva to register the plugin
 */
minervaDefine(function (){
    return {
        register: register,
        unregister: unregister,
        getName: getName,
        getVersion: getVersion
        // ,minWidth: 300
        ,defaultWidth: 450
    }
});

function initPlugin () {

    obtainMainModelId().then(function(){
        // registerListeners();
        const $container = initMainContainer();
        initMainPageStructure($container);
        setupTimers();
    });

}

function registerListeners(){
    // minervaProxy.project.map.addListener({
    //     dbOverlayName: "search",
    //     type: "onSearch",
    //     callback: searchListener
    // });
}

function unregisterListeners() {
    minervaProxy.project.map.removeAllListeners();
}

function obtainMainModelId(){
    return ServerConnector.getModels().then(models => {
        mainModelId = models[0].getId();
    });
}

// ****************************************************************************
// ********************* MINERVA INTERACTION*********************
// ****************************************************************************

function searchListener(entities) {
    // selectedInMap = undefined;
    // if (entities[0].length > 0 && entities[0][0].constructor.name === 'Alias') {
    //     selectedInMap = entities[0][0];
    // }
    // highlightSelectedInTable();
}

function deHighlightAll(){
    // return minervaProxy.project.map.getHighlightedBioEntities().then( highlighted => minervaProxy.project.map.hideBioEntity(highlighted) );
}

// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************

function getContainerClass() {
    return pluginName + '-container';
}

function initMainContainer(){
    $container = $(`<div class="${getContainerClass()}"></div>`).appendTo(pluginContainer);
    return $container;
}

function initMainPageStructure(){

    $container.append(`        
        <div class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!--<div class="modal-header modal-header-primary">-->
                        <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                            <!--<h4 class="modal-title">Interaction with the map not allowed in the guide mode</h4>-->
                    <!--</div>-->
                    <div class="modal-body">                        
                    </div>
                    <!--<div class="modal-footer">-->
                        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    <!--</div>-->
                </div>
            </div>
        </div> 
    `);

    intializeIntroJs();

    $container.append(`   
        <div class="container-fluid"> 
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn-restart btn btn-warning btn-default btn-block">Restart</button>
                </div>
            </div>
        </div>  
             
        `);

    $container.find('.btn-restart').on('click', () => restart() );

    // container.find('.btn-search').on('click', () => search() );
    // container.find('.input-disease').keypress(e => {if (e.which == 13) {search(); return false}} );
}

function restart() {
    cleanMinerva();
    $container.find('.intro-story-container').each(function(){
        cleanStory($(this));
        $(this).find('.panel-collapse').collapse('hide')
    })
}

function initializeIntroJsStoryHtml(id, headerText, description){
    return $(`
        <div class="container-fluid"> 
            <div class="row">
                <div class="col-md-12">
                    <div class="intro-story-container">
                        <div class="panel card panel-primary story border-primary mb-3">
                            <div class="panel-heading card-header bg-primary text-light">${headerText}
                            </div>
                            <div class="panel-body card-body py-2">                            
                                ${description}
                                <span class="pull-right fa fa-chevron-down clickable_space" data-toggle="collapse" data-target="#data_${id}">
                            </div>
                            <div id="data_${id}" class="panel-collapse collapse">
                                <ul class="list-group">                            
                                </ul>
                            </div>
                        </div>                             
                    </div>  
                </div>
            </div>            
        </div>`).appendTo($container);
}

function initializeIntroStoryStepsHtml($introContainer, steps) {
    let i = 1;
    steps.forEach(step => {
        $introContainer.find('ul').append(`<li class="list-group-item">${i}. ${step.intro}</li>`);
        i+=1;
    })
};

function startIntro($introContainer, intro, step = 0) {

    cleanMinerva();
    cleanStory($introContainer);

    //TODO the calc should be changed so that when the calc changes in the minerva code, the plugin still works correctly
    if (settings.padding > 0) {
        pluginContainer.closest('.tab-pane').css('height', `calc(100vh - 42px - ${settings.padding}px)`);
        $(selections.minervaApp).css('padding', `${settings.padding}px`);

        //now we need to let Minerva know that a resize of the map happend which can be done only
        //by simulating what Minerva does when plugin split bar is moved

        $(selections.leftMenuHideButton).click();
        $(selections.leftMenuHideButton).click();
    }

    $(selections.overview).css('pointer-events', 'none');

    // the following had to be implemented in order for the steps, which contain asynchronous code (setTimeout),
    // to run i sequence. Without this, part of step, e.g., 3 could run only after step, e.g., 5
    guideGlobalState.val = 0;
    intro.start();
    for (let i = 1; i <= step; i++) {
        callWithValidState(i, (()=>{intro.nextStep()}))
    }
}


function callWithValidState(state, callback) {
    if (state === guideGlobalState.val) {
        callback();
    } else {
        setTimeout(()=>callWithValidState(state, callback), 50);
    }
}

function intializeIntroJs(){

    let params = {
        initializeIntroJsStoryHtml: initializeIntroJsStoryHtml
        , initializeIntroStoryStepsHtml: initializeIntroStoryStepsHtml
        , startIntro: startIntro
        , addDisabledInteractionMessage: addDisabledInteractionMessage
        , queryMinervaApiMainModel: queryMinervaApiMainModel
        , cleanMinerva:cleanMinerva
        , settings: settings
        , minervaProxy: minervaProxy
        , mainModelId: mainModelId
        ,guideGlobalState:guideGlobalState
    };

    [
        initializeIntroStory(params)
        , initializeStructureStory(params)
        , initializeDrugsStory(params)
    ].forEach( intro => {
        intro.onexit(function () {
            $(selections.overview).css('pointer-events', 'auto');

            if (settings.padding) {
                $(selections.minervaApp).css('padding', '0px');
                //TODO the calc should be changed so that when the calc changes in the minerva code, the plugin still works correctly
                pluginContainer.closest('.tab-pane').css('height', 'calc(100vh - 42px)');

                //now we need to let Minerva know that a resize of the map happend which can be done only
                //by simulating what Minerva does when plugin split bar is moved
                $(selections.leftMenuHideButton).click();
                $(selections.leftMenuHideButton).click();
            }
        });
        introJsInstances.push(intro);
    });
}


function addDisabledInteractionMessage(){
    $(selections.introJsDisableInteractions).click( (event) => {
        $container.find(".modal").modal('show');
    });
}

function cleanStory($introContainer) {
    $introContainer.find('li').each((ix, val) => {
        $(val).removeClass('list-group-item-success');

    });
}

function cleanMinerva(){
    $(selections.clearButton).click();
    $(selections.centerMapLink).click();
    $(selections.molArtCloseButton).click();
    $(selections.dialogs).css('display', 'none');
    $(selections.pathwaysOverlayCheckbox).parent().find('a').click();
    // $('table[name="generalOverlaysTab"] td:contains("Network")').parent().find('a').click();
    $(selections.leftMenuInputs).add(selections.topCheckBoxes).each(function(ix, e) {
        if ($(e).prop('checked')){
            $(e).click();
        }
    });

    $(selections.genericTab)[0].click();
    $(selections.genericSearchTab).click();

    $(selections.dialogCloseButtons).click();
}

function toggleModal(state) {
    pluginContainer.find(".modal").modal(state);
}

function showModal() {
    toggleModal('show');
}

function updateModalHtml(html) {
    pluginContainer.find(".modal .modal-content").html(html)

}

function hideModal() {
    toggleModal('hide');
}

function queryMinervaApiMainModel(apiSuffix) {

    let projectId = minervaProxy.project.data.getProjectId();
    let apiBaseUrl = ServerConnector.getApiBaseUrl();

    return new Promise(function (resolve, reject) {
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", `${apiBaseUrl}projects/${projectId}/models/${mainModelId}/${apiSuffix}`, true);
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                // console.log('resolved', this);
                resolve(JSON.parse(this.responseText))
            }
            if (this.status != 200) {
                // console.log('rejected', this);
                reject(this)
            }
        };

        xhttp.send();

    });
}

function startTimer() {
    // window.setTimeout returns an Id that can be used to start and stop a timer
    mainTimeoutId = window.setTimeout(mainTimeout, settings.inactivityTimeout);
    // console.log('starttimer')
}

function mainTimeout() {
    // console.log('main timeout');
    window.clearTimeout(mainTimeoutId);
    currentConfirmationTimeout = settings.confirmationTimeout;
    showModal();
    restartConfirmationTimeout();
}

function restartConfirmationTimeout() {
    updateModalHtml(`<p>The map will reinitialize in ${currentConfirmationTimeout/1000} s.</p> <p>To cancel the countdown, touch the map or move the mouse over the map.</p>`);
    confirmationTimeoutId = window.setTimeout(confirmationTimeout, settings.confirmationTimeoutRefresh)
}

function confirmationTimeout() {
    window.clearTimeout(confirmationTimeoutId);
    currentConfirmationTimeout -= settings.confirmationTimeoutRefresh;
    // console.log('confirmation timeout')
    if (currentConfirmationTimeout > 0) {
        restartConfirmationTimeout();
    } else {
        restart();
        resetTimer(true);
    }
}

function clearTimeouts() {
    window.clearTimeout(mainTimeoutId);
    window.clearTimeout(confirmationTimeoutId);
}

function resetTimer(full=false) {
    // console.log("reset timer", full)
    if (full){
        introJsInstances.forEach(intro => intro.exit(true));
    }
    clearTimeouts();
    hideModal();
    startTimer();
}

function setupTimers () {
    document.addEventListener("mousemove", () => resetTimer(), false);
    document.addEventListener("mousedown", () => resetTimer(), false);
    document.addEventListener("keypress", () => resetTimer(), false);
    document.addEventListener("touchstart", () => resetTimer(), false);
    document.addEventListener("touchmove", () => resetTimer(), false);

    startTimer();
}