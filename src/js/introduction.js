const introJs = require('intro.js');

const selections = require('./selections');
let guideGlobalState;

const synapseImgCoords = [[4960,1971],[5241,1971],[5241,2163],[4960,2163]];
const toMapImgCoords = [[5188,107], [5536,107 ],[5536,245], [5188,245]];

let re1338 = undefined;

let $ = window.$;
if ($ === undefined) {
    $ = minerva.$;
}

function getRelativePosition(pos) {
    // Obtaining relative position of a point in the overview image which has resolution 5776x4040
    return [pos[0]/5776, pos[1]/4040];
}

const initializeStory = function(params) {

    guideGlobalState = params.guideGlobalState;

    // TODO the follosing needs to be here in order to load image into overview so that selections.overview
    // is valid. This needs to be removed after the overview box becomes identifiable
    $(selections.showOverviewButton).click();
    $(selections.closeOverview).click();

    params.queryMinervaApiMainModel('bioEntities:search?query=reaction:re1338').then(result => {
        return params.minervaProxy.project.data.getBioEntityById({id:result[0].id, modelId:result[0].modelId, type:"REACTION"});
    }).then(entity => {
        re1338 = entity
    });

    const id = 'introduction';
    const $introContainer = params.initializeIntroJsStoryHtml(id, 'Introduction', 'An introduction to the PD map');
    const intro = initializeStoryContet($introContainer, params);

    $introContainer.find('.panel-heading').click((e) => {
        $(e.target).closest('.panel').find('.panel-collapse').collapse('show');
        params.startIntro($introContainer, intro);
    });
    $introContainer.find('.panel.story li').each((i, el) => {
        $(el).click(() => params.startIntro($introContainer, intro, i) );
    });
    return intro;
};

function initializeStoryContet($introContainer, params){

    const intro = introJs();
    const settings = params.settings;

    const steps = [
        { //0
            element: $(selections.minervaMap)[0]
            , intro: "This is Parkinson’s disease map. It encodes known molecular mechanisms involved in PD."
            , disableInteraction: settings.disableInteraction
        },
        { //1
            element: $(selections.showOverviewButton)[0]
            , intro: "The 'Show overview' button opens a graphical, high-level description of the contents."
            , disableInteraction: settings.disableInteraction

        },
        { //2
            element: $(selections.overview)[0]
            , intro: `Take a look at the cell types represented in the map. 
            Clicking on blue rectangles on the image will give you more details about a given structure. 
            The next step will take you to the rightmost rectangle in the middle - the illustration of the synapse.<br><br>
            <b>Note:</b> Feel free to explore yourself at any moment, simply click 'Skip' or escape key. The buttons in 'Show overlay' are not accessible during the guided tour."`
            , disableInteraction: settings.disableInteraction
        },
        { //3
            element: $(selections.overview)[0]
            , intro: `This is a schematic drawing of the synapse.
            Clicking on the “to map” button in the top right corner will take you to the part of the map with the 
            corresponding mechanisms encoded in the map by clicking. We will go there in the next step.<br><br>
            <b>Note:</b> Feel free to explore yourself at any moment, simply click 'Skip' or escape button. The buttons in 'Show overlay' are not accessible during the guided tour.`
            , disableInteraction: settings.disableInteraction
        },
        { //4
            element: $(selections.minervaMap)[0]
            , intro: `We are in the area of the synaptic cleft, with the presynaptic terminal 
            of the dopaminergic neuron to the left. In the next step we will take a look at a single interaction.`
            , disableInteraction: settings.disableInteraction
        },
        { //5
            element: $(selections.minervaMap)[0]
            , intro: `Information about the reaction and its elements can be seen in the left panel. 
            The map is interactive and clicking on reactions or elements will update the contents of the left panel.`
            , disableInteraction: settings.disableInteraction
        },
        { //6
            element: $(selections.leftPanel)[0]
            , intro: `The left panel shows what publications were used to create this interaction, 
            and details about its components. You can scroll down to see all the information. 
            Annotations are hyperlinked, so you can jump directly into the associated web resource.`
            , disableInteraction: settings.disableInteraction
        },
        { //7
            element: $(selections.minervaMap)[0]
            , intro: `As you see, the map is quite large. You can navigate through it, and zoom in and out. 
            Coloured rectangles represent molecular pathways, 
            which become transparent if you zoom in close enough. 
            You can also search for specific elements using the left panel.`
            , disableInteraction: settings.disableInteraction
        },
        { //8
            element: $(selections.leftPanel)[0]
            , intro: `Let’s look for two familial genes, PINK1 and PRKN, in the map. 
            You can look for many elements at a time, separating them with a comma or semicolon. 
            Search works for synonyms too - Parkin’s official gene symbol is PRKN. `
            , disableInteraction: settings.disableInteraction
        },
        { //9
            element: $(selections.minervaMap)[0]
            , intro: `You can see search results in the main area as pins, but and the left panel as a list. 
            Each element has a separate tab and a dedicated color of the pin. 
            It allows you to see where PINK1 and PRKN are in the map, and where they may be interacting.`
            , disableInteraction: settings.disableInteraction
        },
        { //10
            element: $(selections.minervaMap)[0]
            , intro: `This concludes our introduction. Take a look at 
            <a href="https://minerva.pages.uni.lu/doc/user_manual/v12.1/index/#search-tab" target="_blank">the user manual for “Search” function</a> 
            to learn more about advanced search`
            , disableInteraction: settings.disableInteraction
        }
    ];

    intro.setOptions({
        steps: steps,
        showProgress: false
        , showBullets: false
    });
    params.initializeIntroStoryStepsHtml($introContainer, steps);

    intro.onchange(function (target) {

        for (let i = 0; i <= this._currentStep; ++i) {
            $($introContainer.find('li')[i]).addClass('list-group-item-success');
        }
        //handle back button
        $introContainer.find('li').each((i, el) => {
            if (i > this._currentStep){
                $(el).removeClass('list-group-item-success');
            }
        });
    });



    function getRelPos(rect){
        return getRelativePosition([rect[0][0] + (rect[1][0] - rect[0][0]) / 2  , rect[1][1]+(rect[2][1] - rect[1][1]) / 2]);
    }

    intro.onbeforechange(function (target) {

        function carryOutStep(steps) {

            if (steps.indexOf(0) >= 0) {
                guideGlobalState.val = 1;
            }
            if (steps.indexOf(1) >= 0) {
                guideGlobalState.val = 2;
            }
            if (steps.indexOf(2) >= 0) {
                $(selections.showOverviewButton).click();
                guideGlobalState.val = 3;
            }
            if (steps.indexOf(3) >= 0) {
                $(selections.showOverviewButton).click();
                // TODO: renmove setTimeout
                setTimeout(function() {

                    let $img = $(selections.overviewImg);
                    let relPos = getRelPos(synapseImgCoords);
                    let event = $.Event('click');
                    event.clientX = $img.width() * relPos[0];
                    event.clientY = $img.height() * relPos[1];
                    $img.trigger(event);
                    guideGlobalState.val = 4;
                }, 30);
            }
            if (steps.indexOf(4) >= 0) {
                // TODO: remove setTimeout
                // the timeout here is because when this step is carried out after step 5, it needs first
                // to call step 3 which needs to be finished before this step can be carried out
                setTimeout(function() {
                    let $img = $(selections.overviewImg);
                    let relPos = getRelPos(toMapImgCoords);
                    let event = $.Event('click');
                    event.clientX = $img.width() * relPos[0];
                    event.clientY = $img.height() * relPos[1];
                    $img.trigger(event);
                    guideGlobalState.val = 5;
                }, 100);
            }
            if (steps.indexOf(5) >= 0) {
                $(selections.genericSearchInput).val("reaction:re1338");
                $(selections.genericSearchLink).click();
                $(selections.genericSearchInput).val("");
                guideGlobalState.val = 6;
            }
            if (steps.indexOf(6) >= 0) {
                guideGlobalState.val = 7;

            }
            if (steps.indexOf(7) >= 0) {
                $(selections.clearButton).click();
                $(selections.centerMapLink)[0].click();
                guideGlobalState.val = 8;
            }
            if (steps.indexOf(8) >= 0) {
                // $(selections.clearButton).trigger('click', function(){
                //     $(selections.genericSearchInput).val("PINK1,Parkin");
                // });
                $(selections.clearButton).click();
                setTimeout(()=> {
                    $(selections.genericSearchInput).val("PINK1,Parkin")
                        guideGlobalState.val = 9;
                }, 100);
            }
            if (steps.indexOf(9) >= 0) {
                $(selections.genericSearchLink).click();
                guideGlobalState.val = 10;
            }
            if (steps.indexOf(10) >= 0) {
                params.cleanMinerva();
                guideGlobalState.val = 11;
            }
        }

        let stepsToCarryOut = [];
        if (this._direction == 'backward') {
            if (this._currentStep == 1) {
                    $(selections.closeOverview).click();
            } else if (this._currentStep == 4) {
                // stepsToCarryOut.push(2);
                stepsToCarryOut.push(3);
                // new Promise((resolve)=> {carryOutStep(2); resolve()}).then(()=>{carryOutStep(3);});
                // carryOutStep(2);
                // carryOutStep(3);
            } else if (this._currentStep == 6) {
                stepsToCarryOut.push(5);
            } else if (this._currentStep == 9) {
                $(selections.genericSearchInput).val("PINK1,Parkin");
                // $(selections.clearButton).click();
            }
        } else {
            if (this._currentStep == 4) {
                stepsToCarryOut.push(3);
            } else if (this._currentStep == 6) {
                stepsToCarryOut.push(5);
            }
        }

        stepsToCarryOut.push(this._currentStep);

        carryOutStep(stepsToCarryOut);

    });

    intro.onafterchange(function (target) {

        params.addDisabledInteractionMessage();

    });

    return intro;
}

module.exports = initializeStory;