module.exports = {
    minervaApp: '#minervaAppDiv'
    , minervaMap: '.minerva-map'
    , mapCanvas: '.minerva-map canvas'
    , clearButton: 'button[name="clearButton"]'
    , dialogs: 'div[role="dialog"]'
    , pathwaysOverlayCheckbox: 'table[name="generalOverlaysTab"] td:contains("Pathways")'
    , leftMenuInputs: 'table[name="generalOverlaysTab"] input'
    , genericTab: 'div[name="tabView"]:contains("SEARCH") .nav:has(i.fa-search) a'
    , genericSearchTab: 'a:contains("GENERIC")'
    , dialogCloseButtons: '.ui-dialog button[title="Close" i]'
    , topCheckBoxes: '.minerva-top-checkbox-div input[type="checkbox"]'


    , showOverviewButton: '.minerva-overview-button'
    , overview: '.ui-dialog:has("img")' // TODO this needs to be changed for something specific
    , overviewImg: '.ui-dialog img'
    , closeOverview: '.ui-dialog button[title="Close" i]' // TODO this actually selects close button of all dialogs
    , leftPanel: '.minerva-left-panel'
        , centerMapLink: '[title="center map"], a.minerva-center-map-button'
    , genericSearchInput: '.minerva-generic-search-panel input[name="searchInput"], #panel_tab_1 input[name="searchInput"]'
    , genericSearchLink: '.minerva-generic-search-panel a:has("img"), #panel_tab_1  a:has("img"), .minerva-generic-search-panel .minerva-search-link'


    , molArt: '.minerva-molart-container .pv-inner-container'
    , molArtCloseButton: '.minerva-molart-close-button'
    , contextMenu: '.dropdown-menu[name="contextMenu"]'
    , contextMenuMolArt: 'a:contains("MolArt")'
    , prknMolArtLink: 'a:contains("O60260")'
    , introJsFloatingDiv: '.introjs-tooltip.introjs-floating'

    , drugTabLink: 'a:contains("DRUG")'
    , drugTabSearchPanel: '.minerva-drug-panel .searchPanel, #panel_tab_2 .searchPanel'
    , drugTabInput: '.minerva-drug-panel input[name="searchinput" i], .minerva-drug-panel input.tt-input, #panel_tab_2 input.tt-input'
    , drugTabSearchLink: '.minerva-drug-panel  div[name="searchQuery"] a, #panel_tab_2 div[name="searchQuery"] a'

    ,leftMenuHideButton: '.minerva-header-hide-button, .headerHideButton'

    ,introJsDisableInteractions: 'introjs-disableInteraction'
};

