const introJs = require('intro.js');

let prknCoords = undefined;
const zoomLevel = 9;

const selections = require('./selections');
let guideGlobalState;

let $ = window.$;
if ($ === undefined) {
    $ = minerva.$;
}


function getPrknCoordinates(params){
    let compartmentId = undefined;
    params.queryMinervaApiMainModel('bioEntities:search?perfectMatch=false&query=mitochondrial%20motility')
        .then(result => params.minervaProxy.project.data.getBioEntityById(result))
        .then(result => {
            result.forEach(e => {
                // Need to check for pathway or compartment since from version 12.2. some compartments became pathways
                if ((e.getType() === "Compartment" || e.getType() === "Pathway") && e.getModelId() === params.mainModelId) {
                    compartmentId = e.getId();
                }
            });
            return params.queryMinervaApiMainModel('bioEntities:search?query=prkn')
        })
        .then(result => params.minervaProxy.project.data.getBioEntityById(result))
        .then(entities => {

            entities.forEach(e=> {
                if (e.getCompartmentId() === compartmentId) {
                    let c = e.getCenter();
                    prknCoords = [c.x, c.y];
                }
            })
        }, error=>{console.error(error)});
}

const initializeStory = function(params) {

    guideGlobalState = params.guideGlobalState;

    const id = 'protein_structures';
    const $introContainer = params.initializeIntroJsStoryHtml(id, 'View protein structures', 'How to visualize annotated 3D structures');
    const intro = initializeStoryContet($introContainer, params);

    getPrknCoordinates(params);

    $introContainer.find('.panel-heading').click((e) => {
        $(e.target).closest('.panel').find('.panel-collapse').collapse('show');
        params.startIntro($introContainer, intro);
    });
    $introContainer.find('.panel.story li').each((i, el) => {
        $(el).click(() => params.startIntro($introContainer, intro, i) );
    });
    return intro;
};

function initializeStoryContet($introContainer, params){

    const intro = introJs();
    const settings = params.settings;

    const steps = [
        { //0
            element: $(selections.minervaMap)[0]
            , intro: `In the PD map you can take a look at the annotated 3D structures of proteins. 
            The PD map benefits from the integration with the <a href="https://davidhoksza.github.io/MolArt/" target="_blank">MolArt tool</a>. 
            Let’s take a look at an example - PRKN protein`
            , disableInteraction: settings.disableInteraction
        },
        { //1
            element: $(selections.minervaMap)[0]
            , intro: `Right click on the protein will display the contextual menu. 
            You will see the MolArt at the bottom of the menu.`
            , disableInteraction: settings.disableInteraction

        },
        { //2
            element: $(selections.contextMenu)[0]
            , intro: `Clicking on MolArt will show UniProt identifiers for this element. 
            Click on the identifier to show the MolArt window.`
            , disableInteraction: settings.disableInteraction
        },
        { //3
            element: $(selections.minervaApp)[0]
            , intro: `This is the MolArt window, where you can see and explore the 3D structure of 
            PRKN protein, and its annotations.`
            , disableInteraction: settings.disableInteraction
        },
        { //4
            element: $(selections.minervaMap)[0]
            , intro: `This concludes our introduction. Take a look at 
            <a href="https://minerva.pages.uni.lu/doc/user_manual/v12.1/index/#open-molart" target="_blank">the user manual for "MolArt" function</a> to learn more.`
            , disableInteraction: settings.disableInteraction
        }
    ];

    intro.setOptions({
        steps: steps,
        showProgress: false
        , showBullets: false
    });
    params.initializeIntroStoryStepsHtml($introContainer, steps);

    intro.onchange(function (target) {

        for (let i = 0; i <= this._currentStep; ++i) {
            $($introContainer.find('li')[i]).addClass('list-group-item-success');
        }

        //handle back button
        $introContainer.find('li').each((i, el) => {
            if (i > this._currentStep){
                $(el).removeClass('list-group-item-success');
            }
        });

        $('.introjs-tooltip.introjs-floating').css('left', 50)
    });

    intro.onbeforechange(function (target) {

        function carryOutStep(steps) {
            if (steps.indexOf(0) >= 0) {
                guideGlobalState.val = 1;
            }
            if (steps.indexOf(1) >= 0) {
                params.minervaProxy.project.map.setCenter({modelId:params.mainModelId, x:prknCoords[0], y:prknCoords[1]});
                params.minervaProxy.project.map.setZoom({modelId:params.mainModelId, zoom: zoomLevel});
                setTimeout(function() {
                    guideGlobalState.val = 2;
                }, 500);

            }
            if (steps.indexOf(2) >= 0) {
                //TODO: remove setTimeout
                let event = new Event('contextmenu');
                let $canvas = $(selections.mapCanvas);
                minerva.GuiConnector.xPos = $canvas.offset().left + $canvas.width()/2;
                event.clientX = minerva.GuiConnector.xPos;
                minerva.GuiConnector.yPos = $canvas.offset().top + $canvas.height()/2;
                event.clientY = minerva.GuiConnector.yPos ;
                $canvas.parent()[0].dispatchEvent(event);
                //TODO remove setTimeout
                setTimeout(function() {
                    $(selections.contextMenuMolArt).parent().find('ul').css('display', 'block');
                    guideGlobalState.val = 3;
                }, 200);
            }
            if (steps.indexOf(3) >= 0) {

                params.minervaProxy.project.map.setCenter({modelId:params.mainModelId, x:prknCoords[0], y:prknCoords[1]});
                params.minervaProxy.project.map.setZoom({modelId:params.mainModelId, zoom: zoomLevel});

                $(selections.prknMolArtLink).click();
                setTimeout(function() {
                    guideGlobalState.val = 4;
                    //z-index of molart container needs to be decreased so that the z-index of timeout message is above it
                    $('.minerva-molart-container').css('z-index', 100000000);
                }, 2000);
            }
            if (steps.indexOf(4) >= 0) {
                $(selections.molArtCloseButton).click();
                params.cleanMinerva();
                guideGlobalState.val = 5;
            }
        }

        let stepsToCarryOut = [];

        if (this._direction === 'backward') {
            if (this._currentStep === 0) {
                $(selections.centerMapLink).click()
            }
            if (this._currentStep === 2) {
                stepsToCarryOut.push(1);
                $(selections.molArtCloseButton).click();
            }

        }

        stepsToCarryOut.push(this._currentStep);

        carryOutStep(stepsToCarryOut);

    });

    intro.onafterchange(function (target) {

        params.addDisabledInteractionMessage();

        if (this._currentStep == 3){
            setTimeout(function () {
                //it can happen that after 1s, molart plugin is not there any more and in such a case
                //we don't want to do the transition
                if (selections.molArt.length > 0) {

                    $(selections.introJsFloatingDiv).animate({
                        left: '88%'
                        , top: '80%'
                    }, 300);

                }

            }, 500);

        }

    });

    return intro;
}

module.exports = initializeStory;