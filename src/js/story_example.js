const introJs = require('intro.js');

let $ = window.$;
if ($ === undefined) {
    $ = minerva.$;
}

const initializeStory = function(params) {

    const id = 'story_test';
    const $introContainer = params.initializeIntroJsStoryHtml(id, 'Example story', 'Description of the examples story');
    const intro = initializeStoryContet($introContainer, params);

    $introContainer.find('.panel-heading').click((e) => {
        $(e.target).closest('.panel').find('.panel-collapse').collapse('show');
        startIntro($introContainer, intro);
    });
    $introContainer.find('.panel.story li').each((i, el) => {
        $(el).click(() => params.startIntro($introContainer, intro, i) );
    });
    return intro;
};

function initializeStoryContet($introContainer, params){

    const intro = introJs();
    const settings = params.settings;

    const steps = [
        { //0
            intro: "This tutorial will guide you through the basic MINERVA functionality!",
        },
        { //1
            element: $('.minerva-left-panel')[0],
            intro: "Meet the left panel.",
            scrollTo: $("#minervaAppDiv")
            , disableInteraction: settings.disableInteraction

        },
        { //2
            element:  $('.minerva-left-panel .nav.nav-tabs:contains("SEARCH")')[0],
            intro: "Now meet the upper panel using which you can access the search functionality. Let's try it..."
            , disableInteraction: settings.disableInteraction
        },
        { //3
            element: $('div[name="tabView"]:contains("GENERIC")')[1],
            intro: "Oh, snap. Near miss. This is the drug search.... Ok let's try again"
            , disableInteraction: settings.disableInteraction
        },
        { //4
            element: $('div[name="tabView"]:contains("GENERIC")')[1],
            intro: "Here we go"
            , disableInteraction: settings.disableInteraction
        },
        { //5
            element: $('div[name="tabView"]:contains("GENERIC")')[1],
            intro: "Now let's search for a gene. But which one? .... hmmm .... let's try SNCA, aka PARK1, aka PARK4, aka .... you got the idea. One of the most commonly blablabla"
            , disableInteraction: settings.disableInteraction
        },
        { //6
            element: $('div[name="tabView"]:contains("GENERIC")')[1],
            intro: "First we enter the gene name into the search box"
            , disableInteraction: settings.disableInteraction
        },
        { //7
            element: $('#panel_tab_1 a:has("img")')[0],
            intro: "And then we click the search button to get the results"
            , disableInteraction: settings.disableInteraction
        },
        { //8
            element: $('div[name="searchResults"]')[0],
            intro: "Here we see the list containing the search results"
            , disableInteraction: settings.disableInteraction
        },
        { //9
            element: $('.minerva-map')[0],
            intro: "And in the map the search results are highlighted by the icons"
            , disableInteraction: settings.disableInteraction
        }
    ];

    intro.setOptions({
        steps: steps,
        showProgress: true,
        showBullets: false
    });
    params.initializeIntroStoryStepsHtml($introContainer, steps);

    intro.onchange(function (target) {

        $($introContainer.find('li')[this._currentStep]).addClass('list-group-item-success');
        //handle back button
        $introContainer.find('li').each((i, el) => {
            if (i > this._currentStep){
                $(el).removeClass('list-group-item-success');
            }
        })
    });

    intro.onbeforechange(function (target) {

        if (this._direction == 'backward') {
            if (this._currentStep == 6){
                $('#panel_tab_1 input[name="searchInput"]').val("");
                $('#panel_tab_1 a:has("img")').click();
            }
        }

        if (this._currentStep == 3){
            $('a:contains("DRUG")').click();
        } else if (this._currentStep == 4){
            $('a:contains("GENERIC")').click();
        }
    });

    intro.onafterchange(function (target) {

        params.addDisabledInteractionMessage();

        if (this._currentStep == 7){
            $('#panel_tab_1 input[name="searchInput"]').val("PRKN")
        } else if (this._currentStep == 8){
            $('#panel_tab_1 a:has("img")').click();
        }

    });

    return intro;
}

module.exports = initializeStory;