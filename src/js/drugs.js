const introJs = require('intro.js');

let $ = window.$;
if ($ === undefined) {
    $ = minerva.$;
}

const selections = require('./selections');
let guideGlobalState;

const initializeStory = function(params) {

    guideGlobalState = params.guideGlobalState;

    const id = 'drug_targets';
    const $introContainer = params.initializeIntroJsStoryHtml(id, 'Drug targets', 'See what drugs target specific elements of the map');
    const intro = initializeStoryContet($introContainer, params);

    $introContainer.find('.panel-heading').click((e) => {
        $(e.target).closest('.panel').find('.panel-collapse').collapse('show');
        params.startIntro($introContainer, intro);
    });
    $introContainer.find('.panel.story li').each((i, el) => {
        $(el).click(() => params.startIntro($introContainer, intro, i) );
    });
    return intro;
};

function initializeStoryContet($introContainer, params){

    const intro = introJs();
    const settings = params.settings;

    const steps = [
        { //0
            element: $(selections.leftPanel)[0]
            , intro: `Search functionality allows you to search for elements in the map, 
            but also for drugs targeting these elements. Take a look at the "Drug" subtab in the left panel.`
            , disableInteraction: settings.disableInteraction
        },
        { //1
            element: $(selections.drugTabSearchPanel)[0]
            , intro: `Drug name in the search field will be looked up in 
            <a href="https://www.drugbank.ca/" target="_blank">DrugBank</a> and 
            <a href="https://www.ebi.ac.uk/chembl/" target="_blank">ChEMBL</a> databases, 
            and their targets will be displayed in the left panel as a list, and in the map, as pins. 
            Let’s take a look at the targets of levodopa and carbidopa, two drugs often used in PD therapy.`
            , disableInteraction: settings.disableInteraction
        } ,
        { //2
            element: $(selections.drugTabSearchPanel)[0]
            , intro: `You can enter multiple terms, separating them with a semicolon. 
            Clicking on the search icon (magnifying glass) or pressing enter will execute the query. 
            Go to the next step to see the outcome.`
            , disableInteraction: settings.disableInteraction
        } ,
        { //3
            element: $(selections.minervaMap)[0]
            , intro: `On the map you can see the targets of levodopa and carbidopa, 
            marked by pins of two colors. Levodopa targets dopamine 
            receptors at the synapse, and carbidopa targets DDC.`
            , disableInteraction: settings.disableInteraction
        },
        { //4
            element: $(selections.leftPanel)[0]
            , intro: `All results are shown as a list for each searched drugs. There’s a description of the drug, 
            including its brand names and information if it crosses the blood-brain barrier. 
            Scroll down the list to see the targets of the drugs that are not in the map (no pin).`
            , disableInteraction: settings.disableInteraction
        },
        { //4
            element: $(selections.minervaMap)[0]
            , intro: `This concludes our introduction. Take a look at 
            <a href="https://minerva.pages.uni.lu/doc/user_manual/v12.1/index/#drug-target" target="_blank">the user manual for drug search</a> to learn more`
            , disableInteraction: settings.disableInteraction
        }
    ];

    intro.setOptions({
        steps: steps,
        showProgress: false
        , showBullets: false
    });
    params.initializeIntroStoryStepsHtml($introContainer, steps);

    intro.onchange(function (target) {

        for (let i = 0; i <= this._currentStep; ++i) {
            $($introContainer.find('li')[i]).addClass('list-group-item-success');
        }

        //handle back button
        $introContainer.find('li').each((i, el) => {
            if (i > this._currentStep){
                $(el).removeClass('list-group-item-success');
            }
        });
    });

    intro.onbeforechange(function (target) {

        function carryOutStep(steps) {
            if (steps.indexOf(0) >= 0) {
                params.cleanMinerva();
                setTimeout(function () {
                    $(selections.drugTabLink).click();
                    $(selections.drugTabInput).val("");
                    guideGlobalState.val = 1;
                }, 200);
            }
            if (steps.indexOf(1) >= 0) {
                guideGlobalState.val = 2;
            }
            if (steps.indexOf(2) >= 0) {
                let method = $(selections.drugTabSearchLink)[0].onclick;
                if (method === null || method === undefined) {
                    method = $(selections.drugTabSearchLink)[0].click;
                }
                Promise.resolve(method()).then(function () {
                    $(selections.drugTabInput).val("levodopa;carbidopa");
                });
                guideGlobalState.val = 3;

            }
            if (steps.indexOf(3) >= 0) {
                $(selections.drugTabLink).click();
                $(selections.drugTabSearchLink)[0].click();
                guideGlobalState.val = 4;
            }
            if (steps.indexOf(4) >= 0) {
                guideGlobalState.val = 5;
            }
        }

        let stepsToCarryOut = [];

        if (this._direction == 'backward') {
            if (this._currentStep == 1) {
                stepsToCarryOut.push(0)
            }

        }

        stepsToCarryOut.push(this._currentStep);

        carryOutStep(stepsToCarryOut);

    });

    intro.onafterchange(function (target) {

        params.addDisabledInteractionMessage();

    });

    return intro;
}

module.exports = initializeStory;