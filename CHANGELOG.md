# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2021-11-10
### Fixed
- support for minerva 16.0

## [1.0.1] - 2020-02-05
### Fixed
- support for minerva 15.0

## [1.0.0] - 2019-10-08
### Added
- Introduction to PD map
- How to visualize annotated 3D structures
- See what drugs target specific elements of the map